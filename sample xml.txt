{
    "description": "",
    "faction": "rebel",
    "name": "Force_Unleashed",
    "pilots": [{
        "name": "poedameron",
        "points": 39,
        "ship": "t70xwing",
        "upgrades": {
            "torpedo": ["protontorpedoes"],
            "amd": ["bb8"],
            "tech": ["weaponsguidance"],
            "mod": ["integratedastromech"]
        }
    }, {
        "name": "bluesquadronnovice",
        "points": 31,
        "ship": "t70xwing",
        "upgrades": {
            "torpedo": ["protontorpedoes"],
            "mod": ["hullupgrade"]
        }
    }, {
        "name": "redsquadronveteran",
        "points": 30,
        "ship": "t70xwing",
        "upgrades": {
            "torpedo": ["protontorpedoes"]
        }
    }, {
        "name": "lothal",
        "points": 133,
        "ship": "vcx100",
        "upgrades": {
            "system": ["enhancedscopes"],
            "turret": ["twinlaserturret"],
            "torpedo": ["advprotontorpedoes", "protontorpedoes"],
            "crew": ["sabinewren", "lukeskywalker"],
            "bomb": ["proximitymines"],
            "mod": ["shieldupgrade"],
            "title": ["ghost"]
        }
    }, {
        "name": "ezrabridger",
        "points": 36,
        "ship": "attackshuttle",
        "upgrades": {
            "ept": ["opportunist"],
            "turret": ["ioncannonturret"],
            "crew": ["kylekatarn"],
            "mod": ["shieldupgrade"],
            "title": ["phantom"]
        }
    }],
    "points": 269,
    "vendor": {
        "yasb": {
            "builder": "(Yet Another) X-Wing Miniatures Squad Builder",
            "builder_url": "http://geordanr.github.io/xwing/",
            "link": "http://geordanr.github.io/xwing/?f=Rebel%20Alliance&d=v4!s!175:-1,1,148,147:-1:20:;178:1,-1,-1:-1:6:;177:-1,1,-1,-1:-1:-1:;158:71,135,34,1,166,31:25:2:U.28;161:49,0,74:26:2:&sn=Force_Unleashed"
        }
    },
    "version": "0.3.0"
}