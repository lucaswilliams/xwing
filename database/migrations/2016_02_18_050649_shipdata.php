<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Shipdata extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $rootdir = env('root_dir');

        //Insert ship, pilot and upgrade data
        $data = file_get_contents($rootdir.'/dev/parser/data/upgrades.json');
        //echo '<pre>'; echo $data; echo '</pre>';

        $json = json_decode($data);

        $upgradearray = array();
        $find = array ('advancedtargetingcomputer',
            'advancedcloakingdevice',
            'advancedprotontorpedoes',
            'advancedslam',
            'advancedhomingmissiles',
            'advancedsensors');
        $replace = array('advtargetingcomputer',
            'advancedcloakingdevice',
            'advprotontorpedoes',
            'advancedslam',
            'advhomingmissiles',
            'advancedsensors');
        foreach($json as $upgrade) {
            $stub = str_replace($find, $replace, strtolower(preg_replace('/[^A-Za-z0-9]/', '', $upgrade->name)));
            $upgradearray[] = array(
                'upgrade_name' => $upgrade->name,
                'upgrade_slot' => $upgrade->slot,
                'upgrade_points' => $upgrade->points,
                'upgrade_attack' => (isset($upgrade->attack) ? $upgrade->attack : 0),
                'upgrade_text' => $upgrade->text,
                'upgrade_image' => (isset($upgrade->image) ? $upgrade->image : ''),
                'upgrade_range' => (isset($upgrade->range) ? $upgrade->range : ''),
                'upgrade_stub' => $stub
            );
        }

        DB::table('upgrades')
            ->insert($upgradearray);

        $data = file_get_contents($rootdir.'/dev/parser/data/ships.json');

        $json = json_decode($data);

        $shiparray = array();
        foreach($json as $ship) {
            switch($ship->size) {
                case 'small' : $size = 0; break;
                case 'large' : $size = 1; break;
                case 'huge' : $size = 2; break;
            }

            $stub = strtolower(preg_replace('/[^A-Za-z0-9]/', '', $ship->name));

            $shiparray[] = array(
                'ship_name' => $ship->name,
                'ship_attack' => (isset($ship->attack) ? $ship->attack : 0),
                'ship_agility' => (isset($ship->agility) ? $ship->agility : 0),
                'ship_hull' => (isset($ship->hull) ? $ship->hull : 0),
                'ship_shields' => (isset($ship->shields) ? $ship->shields : 0),
                'ship_moves' => (isset($ship->maneuvers) ? json_encode($ship->maneuvers) : '[]'),
                'ship_moves_energy' => (isset($ship->maneuvers_energy) ? json_encode($ship->maneuvers_energy) : '[]'),
                'ship_size' => $size,
                'ship_stub' => $stub
            );
        }

        DB::table('ships')
            ->insert($shiparray);

        $data = file_get_contents($rootdir.'/dev/parser/data/pilots.json');

        $json = json_decode($data);

        $pilotarray = array();
        foreach($json as $pilot) {
            $ship_id = DB::table('ships')
                ->where('ship_name', '=', $pilot->ship)
                ->get()[0]->ship_id;

            $stub = strtolower(preg_replace('/[^A-Za-z0-9]/', '', $pilot->name));

            $pilotarray[] = array(
                'ship_id' => $ship_id,
                'pilot_name' => $pilot->name,
                'pilot_unique' => (isset($pilot->unique) ? $pilot->unique : false),
                'pilot_skill' => $pilot->skill,
                'pilot_points' => $pilot->points,
                'pilot_slots' => (isset($pilot->slots) ? json_encode($pilot->slots) : '[]'),
                'pilot_text' => (isset($pilot->text) ? $pilot->text : ''),
                'pilot_image' => (isset($pilot->image) ? $pilot->image : ''),
                'pilot_stub' => $stub
            );
        }

        DB::table('pilots')
            ->insert($pilotarray);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
