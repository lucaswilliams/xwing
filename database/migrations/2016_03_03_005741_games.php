<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Games extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function(Blueprint $table) {
            $table->increments('game_id');
            $table->integer('game_initiative');
            $table->integer('game_phase')->default(0);
            $table->longText('squad_1_json');
            $table->longText('squad_2_json');
        });

        Schema::create('game_logs', function(Blueprint $table) {
            $table->increments('logs_id');
            $table->integer('game_id');
            $table->dateTime('logs_time');
            $table->longText('squad_1_json');
            $table->longText('squad_2_json');
            $table->longText('logs_description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('game_logs');
        Schema::drop('games');
    }
}
