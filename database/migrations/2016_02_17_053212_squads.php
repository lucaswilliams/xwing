<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Squads extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('upgrades', function(Blueprint $table) {
            $table->increments('upgrade_id');
            $table->string('upgrade_name', 100);
            $table->string('upgrade_slot', 100);
            $table->integer('upgrade_points');
            $table->integer('upgrade_attack')->nullable();
            $table->text('upgrade_text');
            $table->string('upgrade_image', 200);
            $table->string('upgrade_range', 20)->nullable();
            $table->string('upgrade_stub', 100);
        });

        Schema::create('ships', function(Blueprint $table) {
            $table->increments('ship_id');
            $table->string('ship_name', 50);
            $table->integer('ship_attack');
            $table->integer('ship_agility');
            $table->integer('ship_hull');
            $table->integer('ship_shields');
            $table->string('ship_moves', 200);
            $table->string('ship_moves_energy', 200)->nullable();
            $table->integer('ship_size');
            $table->string('ship_stub', 100);
        });

        Schema::create('pilots', function(Blueprint $table) {
            $table->increments('pilot_id');
            $table->integer('ship_id');
            $table->string('pilot_name', 100);
            $table->boolean('pilot_unique');
            $table->integer('pilot_skill');
            $table->integer('pilot_points');
            $table->string('pilot_slots', 200);
            $table->text('pilot_text');
            $table->string('pilot_image', 200);
            $table->string('pilot_stub', 100);
        });

        Schema::create('squads', function(Blueprint $table) {
            $table->increments('squad_id');
            $table->integer('user_id');
            $table->string('squad_name', 100);
            $table->integer('squad_faction');
            $table->integer('squad_points');
        });

        Schema::create('squad_pilots', function(Blueprint $table) {
            $table->increments('spilot_id');
            $table->integer('squad_id');
            $table->integer('pilot_id');
        });

        Schema::create('squad_upgrades', function(Blueprint $table) {
            $table->increments('supgrade_id');
            $table->integer('spilot_id');
            $table->integer('squad_id');
            $table->integer('upgrade_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('upgrades');
        Schema::drop('pilots');
        Schema::drop('ships');

        Schema::drop('squad_upgrades');
        Schema::drop('squad_pilots');
        Schema::drop('squads');
    }
}
