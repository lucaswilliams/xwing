<?php
	include('header.php');
?>
	<div class="container">
		<div class="col-xs-12">
<?php
	$inputXWS = $_POST['xws'];
	$xwsjson = json_decode($inputXWS, true);
	
	//echo '<pre>'; var_dump($xwsjson); echo '</pre>';
	
	echo '<h1 class="'.$xwsjson['faction'].'">'.$xwsjson['name'].' ('.$xwsjson['points'].')</h1>';
	if(strlen($xwsjson['description'] > 0)) {
		echo '<p>'.$xwsjson['description'].'</p>';
	}
	echo '<p><a href="'.reset($xwsjson['vendor'])['link'].'" target="_blank">View/Edit Build</a></p>';
	
	foreach($xwsjson['pilots'] as $ship) {
		echo '<div class="col-md-6">';
		echo '<h2>'.$ship['ship'].' ('.$ship['points'].')</h2>';
		echo '<h3>'.$ship['name'].'</h3>';
		echo '<h4>Upgrades</h4>';
		foreach($ship['upgrades'] as $key=>$value) {
			echo '<p><strong>'.$key.'</strong><ul>';
			foreach($value as $val) {
				echo '<li>'.$val.'</li>';
			}
			echo '</ul>';
		}
		echo '</p>';
		echo '</div>';
	}
?>
		</div>
	</div>
<?php	
	include('footer.php');
?>