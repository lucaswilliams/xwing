<?php
    $data = file_get_contents((__DIR__).'/data/upgrades.json');
    //echo '<pre>'; echo $data; echo '</pre>';

    $json = json_decode($data);

    //echo '<pre>'; var_dump($json); echo '</pre>';

    $upgradearray = array();
    foreach($json as $upgrade) {
        $upgradearray[] = array(
            'upgrade_name' => $upgrade->name,
            'upgrade_slot' => $upgrade->slot,
            'upgrade_points' => $upgrade->points,
            'upgrade_attack' => (isset($upgrade->attack) ? $upgrade->attack : 0),
            'upgrade_text' => $upgrade->text,
            'upgrade_image' => (isset($upgrade->image) ? $upgrade->image : ''),
            'upgrade_range' => (isset($upgrade->range) ? $upgrade->range : ''),
            'upgrade_stub' => strtolower(preg_replace('/[^A-Za-z0-9]/', '', $upgrade->name))
        );
    }

    echo '<pre>'; var_dump($upgradearray); echo '</pre>';