<?php
include('header.php');
?>	
<div class="container">	
	<div class="row">
		<div class="col-xs-12">
			<form method="POST" action="process.php">
				<label for="xws">Paste your XWS details here:</label>
				<textarea name="xws" id="xws"></textarea>
				<button type="submit" class="btn btn-success">Submit</button>
			</form>
		</div>
	</div>
</div>

<?php
include('footer.php');
?>