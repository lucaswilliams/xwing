@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1>Access Denied</h1>
                <p><?php echo $message; ?></p>
            </div>
        </div>
    </div>
@endsection
