@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <form method="POST">
                {{ csrf_field() }}

                <div class="form-group">
                <label for="squad_1">First squad:</label>
                    <select name="squad_1" id="squad_1" class="form-control search squad">
                        <option></option>
                    <?php
                        foreach($squads as $user => $squad) {
                            echo '<optgroup label="'.$user.'">';
                            foreach($squad as $sq) {
                                echo '<option value="'.$sq->squad_id.'" data-points="'.$sq->squad_points.'" data-faction="'.$sq->squad_faction.'">'.$sq->squad_name.' ('.$sq->squad_points.')</option>';
                            }
                            echo '</optgroup>';
                        }
                    ?>
                    </select>
                    <label for="initiative_1"><input type="radio" name="initiative" value="1" id="initiative_1"> Squad has initiative</label>
                </div>

                <div class="form-group">
                    <label for="squad_2">Second squad:</label>
                    <select name="squad_2" id="squad_2" class="form-control search squad">
                        <option></option>
                        <?php
                        foreach($squads as $user => $squad) {
                            echo '<optgroup label="'.$user.'">';
                            foreach($squad as $sq) {
                                echo '<option value="'.$sq->squad_id.'" data-points="'.$sq->squad_points.'" data-faction="'.$sq->squad_faction.'">'.$sq->squad_name.' ('.$sq->squad_points.')</option>';
                            }
                            echo '</optgroup>';
                        }
                        ?>
                    </select>
                    <label for="initiative_2"><input type="radio" name="initiative" value="2" id="initiative_2"> Squad has initiative</label>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-success"><i class="fa fa-arrow-right"></i> Start game</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
