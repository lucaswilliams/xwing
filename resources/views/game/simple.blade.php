<?php
    $squad_1 = json_decode($game->squad_1_json, true);
    $squad_2 = json_decode($game->squad_2_json, true);
?>

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xs-12" id="phase">
            <h1><?php echo $game->game_phase; ?></h1>
        </div>
        <div class="col-sm-6" id="squad_1">
            <h2><?php echo $squad_1['squad_name']; ?></h2>
            <?php
                foreach($squad_1['pilots'] as $s1ships) {
                    echo '<a data-move="'.urlencode($s1ships['ship_moves']).'" class="game_ship"><h3>'.$s1ships['pilot_name'].'</h3></a>';
                }
            ?>
        </div>
        <div class="col-sm-6" id="squad_2">
            <h2><?php echo $squad_2['squad_name']; ?></h2>
            <?php
                foreach($squad_2['pilots'] as $s2ships) {
                    echo '<a data-move="'.urlencode($s2ships['ship_moves']).'" class="game_ship"><h3>'.$s2ships['pilot_name'].'</h3></a>';
                }
            ?>
        </div>
        <div class="col-sm-6 col-sm-offset-3" id="movement">
            <?php for($i = 5; $i > 0; $i--) { ?>
            <div class="move-row" data-row="<?php echo $i; ?>">
                <div class="move-cell"><h3><?php echo $i; ?></h3></div>
                <div class="move-cell" data-col="0">
                    <img src="{{ asset('img/icons/move-turn-left-green.png') }}" class="green">
                    <img src="{{ asset('img/icons/move-turn-left-red.png') }}" class="red">
                    <img src="{{ asset('img/icons/move-turn-left-white.png') }}" class="white">
                </div>
                <div class="move-cell" data-col="1">
                    <img src="{{ asset('img/icons/move-bank-left-green.png') }}" class="green">
                    <img src="{{ asset('img/icons/move-bank-left-red.png') }}" class="red">
                    <img src="{{ asset('img/icons/move-bank-left-white.png') }}" class="white">
                </div>
                <div class="move-cell" data-col="2">
                    <img src="{{ asset('img/icons/move-straight-green.png') }}" class="green">
                    <img src="{{ asset('img/icons/move-straight-red.png') }}" class="red">
                    <img src="{{ asset('img/icons/move-straight-white.png') }}" class="white">
                </div>
                <div class="move-cell" data-col="3">
                    <img src="{{ asset('img/icons/move-bank-right-green.png') }}" class="green">
                    <img src="{{ asset('img/icons/move-bank-right-red.png') }}" class="red">
                    <img src="{{ asset('img/icons/move-bank-right-white.png') }}" class="white">
                </div>
                <div class="move-cell" data-col="4">
                    <img src="{{ asset('img/icons/move-turn-right-green.png') }}" class="green">
                    <img src="{{ asset('img/icons/move-turn-right-red.png') }}" class="red">
                    <img src="{{ asset('img/icons/move-turn-right-white.png') }}" class="white">
                </div>
                <div class="move-cell" data-col="5">
                    <img src="{{ asset('img/icons/move-koiogran-turn-green.png') }}" class="green">
                    <img src="{{ asset('img/icons/move-koiogran-turn-red.png') }}" class="red">
                    <img src="{{ asset('img/icons/move-koiogran-turn-white.png') }}" class="white">
                </div>
                <div class="move-cell" data-col="6">
                    <img src="{{ asset('img/icons/move-segnor-left-red.png') }}" class="red">
                </div>
                <div class="move-cell" data-col="7">
                    <img src="{{ asset('img/icons/move-segnor-right-red.png') }}" class="red">
                </div>
                <div class="move-cell" data-col="8">
                    <img src="{{ asset('img/icons/move-tallon-left-red.png') }}" class="red">
                </div>
                <div class="move-cell" data-col="9">
                    <img src="{{ asset('img/icons/move-tallon-right-red.png') }}" class="red">
                </div>
            </div>
            <?php } ?>
            <div class="move-row" data-id="0">
                <div class="move-cell"><h3>0</h3></div>
                <div class="move-cell"></div>
                <div class="move-cell"></div>
                <div class="move-cell"><img src="{{ asset('img/icons/move-stop-red.png') }}" class="red"></div>
                <div class="move-cell"></div>
                <div class="move-cell"></div>
                <div class="move-cell"></div>
                <div class="move-cell"></div>
                <div class="move-cell"></div>
                <div class="move-cell"></div>
                <div class="move-cell"></div>
            </div>
        </div>
        <input type="hidden" id="current_ship">
        <div class="col-sm-3" id="movement-list"></div>
    </div>
</div>
@endsection
