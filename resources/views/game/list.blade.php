@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-right">
                <a href="{{ URL::to('/squads/add') }}" class="btn btn-success"><i class="fa fa-plus"></i> Add new</a>
            </div>
            <div class="col-xs-12">
                <table width="100%">
                    <tr>
                        <th>Squad 1</th>
                        <th>Squad 2</th>
                        <th>Phase</th>
                        <th>Actions</th>
                    </tr>
                    <?php
                        foreach($games as $game) {
                            $squad1 = json_decode($game->squad_1_json);
                            $squad2 = json_decode($game->squad_2_json);
                    ?>
                        <tr>
                            <td><?php echo $squad1->squad_name; ?></td>
                            <td><?php echo $squad2->squad_name; ?></td>
                            <td><?php echo ($game->game_phase == 0) ? 'Activation' : 'Combat'; ?></td>
                            <td>
                                <a href="game/play/<?php echo $game->game_id; ?>" class="btn btn-default"><i class="fa fa-play"></i></a>
                                <a href="game/delete/<?php echo $game->game_id; ?>" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>
                            </td>
                        </tr>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div>
@endsection