<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Game in progress | X-Wing Streamer</title>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="{{ asset('fancybox/jquery.fancybox.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/game.css') }}">
</head>
<body>
<div class="game" id="gamearea">
    <div class="header">
        <?php echo ($game->game_phase == 0) ? 'Activation' : 'Combat'; ?> Phase
        <input type="hidden" id="game_id" value="<?php echo $game->game_id; ?>">
    </div>
    <div class="player" id="player1">
        <?php
            $s1 = json_decode($game->squad_1_json, true);

            foreach($s1['pilots'] as $pilot) {
        ?>
        <div class="ship" data-move="<?php echo urlencode($pilot['ship_moves']); ?>" data-pilot="<?php echo $pilot['pilot_name']; ?>">
            <div class="img"><img src="http://localhost:8080/xwing/public/img/<?php echo $pilot['pilot_image']; ?>"></div>
            <div class="status">
                <img src="http://localhost:8080/xwing/public/img/tokens/target-blue.png">
                <img src="http://localhost:8080/xwing/public/img/tokens/evade.png">
                <img src="http://localhost:8080/xwing/public/img/tokens/evade.png">
                <img src="http://localhost:8080/xwing/public/img/tokens/focus.png">
                <img src="http://localhost:8080/xwing/public/img/tokens/weapons-disabled.png">
            </div>
            <div class="stats">
                <div class="stat-hull">
                    <span class="current">3</span>/<?php echo $pilot['ship_hull']; ?>
                </div>
                <div class="stat-shield">
                    <span class="current">3</span>/<?php echo $pilot['ship_shields']; ?>
                </div>
            </div>
            <div class="upgrades">
                <div class="upgrade"><img src="http://localhost:8080/xwing/public/img/icons/upgrade-astromech.png"></div>
                <div class="upgrade"><img src="http://localhost:8080/xwing/public/img/icons/upgrade-elite.png"></div>
                <div class="upgrade"><img src="http://localhost:8080/xwing/public/img/icons/upgrade-crew.png"></div>
                <div class="upgrade"><img src="http://localhost:8080/xwing/public/img/icons/upgrade-bomb.png"></div>
                <div class="upgrade"><img src="http://localhost:8080/xwing/public/img/icons/upgrade-astromech.png"></div>
                <div class="upgrade"><img src="http://localhost:8080/xwing/public/img/icons/upgrade-elite.png"></div>
                <div class="upgrade"><img src="http://localhost:8080/xwing/public/img/icons/upgrade-crew.png"></div>
                <div class="upgrade"><img src="http://localhost:8080/xwing/public/img/icons/upgrade-bomb.png"></div>
            </div>
        </div>
        <?php
            }
        ?>
    </div>
    <div id="game_board" class="board">

    </div>
    <div class="player" id="player2">
        <?php
            $s2 = json_decode($game->squad_2_json, true);

            foreach($s2['pilots'] as $pilot) {
        ?>
        <div class="ship" data-move="<?php echo urlencode($pilot['ship_moves']); ?>" data-pilot="<?php echo $pilot['pilot_name']; ?>">
            <div class="img"><img src="http://localhost:8080/xwing/public/img/<?php echo $pilot['pilot_image']; ?>"></div>
            <div class="status">
                <img src="http://localhost:8080/xwing/public/img/tokens/target-blue.png">
                <img src="http://localhost:8080/xwing/public/img/tokens/evade.png">
                <img src="http://localhost:8080/xwing/public/img/tokens/evade.png">
                <img src="http://localhost:8080/xwing/public/img/tokens/focus.png">
                <img src="http://localhost:8080/xwing/public/img/tokens/weapons-disabled.png">
            </div>
            <div class="stats">
                <div class="stat-hull">
                    <span class="current">3</span>/<?php echo $pilot['ship_hull']; ?>
                </div>
                <div class="stat-shield">
                    <span class="current">3</span>/<?php echo $pilot['ship_shields']; ?>
                </div>
            </div>
            <div class="upgrades">
                <div class="upgrade"><img src="http://localhost:8080/xwing/public/img/icons/upgrade-astromech.png"></div>
                <div class="upgrade"><img src="http://localhost:8080/xwing/public/img/icons/upgrade-elite.png"></div>
                <div class="upgrade"><img src="http://localhost:8080/xwing/public/img/icons/upgrade-crew.png"></div>
                <div class="upgrade"><img src="http://localhost:8080/xwing/public/img/icons/upgrade-bomb.png"></div>
                <div class="upgrade"><img src="http://localhost:8080/xwing/public/img/icons/upgrade-astromech.png"></div>
                <div class="upgrade"><img src="http://localhost:8080/xwing/public/img/icons/upgrade-elite.png"></div>
                <div class="upgrade"><img src="http://localhost:8080/xwing/public/img/icons/upgrade-crew.png"></div>
                <div class="upgrade"><img src="http://localhost:8080/xwing/public/img/icons/upgrade-bomb.png"></div>
            </div>
        </div>
        <?php
            }
        ?>
    </div>
</div>
<div id="controls" class="row">
    <div class="col-sm-4" id="movement">
        <?php for($i = 5; $i > 0; $i--) { ?>
        <div class="move-row" data-row="<?php echo $i; ?>">
            <div class="move-cell"><h3><?php echo $i; ?></h3></div>
            <div class="move-cell" data-col="0">
                <img src="{{ asset('img/icons/move-turn-left-green.png') }}" class="green">
                <img src="{{ asset('img/icons/move-turn-left-red.png') }}" class="red">
                <img src="{{ asset('img/icons/move-turn-left-white.png') }}" class="white">
            </div>
            <div class="move-cell" data-col="1">
                <img src="{{ asset('img/icons/move-bank-left-green.png') }}" class="green">
                <img src="{{ asset('img/icons/move-bank-left-red.png') }}" class="red">
                <img src="{{ asset('img/icons/move-bank-left-white.png') }}" class="white">
            </div>
            <div class="move-cell" data-col="2">
                <img src="{{ asset('img/icons/move-straight-green.png') }}" class="green">
                <img src="{{ asset('img/icons/move-straight-red.png') }}" class="red">
                <img src="{{ asset('img/icons/move-straight-white.png') }}" class="white">
            </div>
            <div class="move-cell" data-col="3">
                <img src="{{ asset('img/icons/move-bank-right-green.png') }}" class="green">
                <img src="{{ asset('img/icons/move-bank-right-red.png') }}" class="red">
                <img src="{{ asset('img/icons/move-bank-right-white.png') }}" class="white">
            </div>
            <div class="move-cell" data-col="4">
                <img src="{{ asset('img/icons/move-turn-right-green.png') }}" class="green">
                <img src="{{ asset('img/icons/move-turn-right-red.png') }}" class="red">
                <img src="{{ asset('img/icons/move-turn-right-white.png') }}" class="white">
            </div>
            <div class="move-cell" data-col="5">
                <img src="{{ asset('img/icons/move-koiogran-turn-green.png') }}" class="green">
                <img src="{{ asset('img/icons/move-koiogran-turn-red.png') }}" class="red">
                <img src="{{ asset('img/icons/move-koiogran-turn-white.png') }}" class="white">
            </div>
            <div class="move-cell" data-col="6">
                <img src="{{ asset('img/icons/move-segnor-left-red.png') }}" class="red">
            </div>
            <div class="move-cell" data-col="7">
                <img src="{{ asset('img/icons/move-segnor-right-red.png') }}" class="red">
            </div>
            <div class="move-cell" data-col="8">
                <img src="{{ asset('img/icons/move-tallon-left-red.png') }}" class="red">
            </div>
            <div class="move-cell" data-col="9">
                <img src="{{ asset('img/icons/move-tallon-right-red.png') }}" class="red">
            </div>
        </div>
        <?php } ?>
        <div class="move-row" data-id="0">
            <div class="move-cell"><h3>0</h3></div>
            <div class="move-cell"></div>
            <div class="move-cell"></div>
            <div class="move-cell"><img src="{{ asset('img/icons/move-stop-red.png') }}" class="red"></div>
            <div class="move-cell"></div>
            <div class="move-cell"></div>
            <div class="move-cell"></div>
            <div class="move-cell"></div>
            <div class="move-cell"></div>
            <div class="move-cell"></div>
            <div class="move-cell"></div>
        </div>
    </div>
    <input type="hidden" id="current_ship">
    <div class="col-sm-6" id="movement-list"></div>
    <div class="col-sm-2"><button id="end-turn" class="btn btn-success"><h1><i class="fa fa-floppy-o"></i> Save turn</h1></button></div>
</div>
<script type="text/javascript" src="https://code.jquery.com/jquery-2.2.1.min.js"></script>
<script src="{{ asset('fancybox/jquery.fancybox.pack.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/webcam.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/html2canvas.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/html2canvas.svg.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/canvas2image.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/game.js') }}"></script>
</body>
</html>