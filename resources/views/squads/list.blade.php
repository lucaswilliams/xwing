@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-right">
                <a href="{{ URL::to('/squads/add') }}" class="btn btn-success"><i class="fa fa-plus"></i> Add new</a>
            </div>
            <div class="col-xs-12">
                <table width="100%">
                    <tr>
                        <th>Name</th>
                        <th>Faction</th>
                        <th>Points</th>
                        <th>User</th>
                        <th>Actions</th>
                    </tr>
                    <?php foreach($squads as $squad) { ?>
                        <tr>
                            <td><?php echo $squad->squad_name; ?></td>
                            <td><?php
                                switch($squad->squad_faction) {
                                    case 1: echo 'Rebel'; break;
                                    case 2: echo 'Imperial'; break;
                                    case 3: echo 'Scum'; break;
                                    default: echo 'Unknown'; break;
                                }
                            ?></td>
                            <td><?php echo $squad->squad_points; ?></td>
                            <td><?php echo $squad->name; ?></td>
                            <td>
                                <a href="squads/<?php echo $squad->squad_id; ?>" class="btn btn-default"><i class="fa fa-pencil"></i></a>
                                <a href="squads/delete/<?php echo $squad->squad_id; ?>" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>
                            </td>
                        </tr>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div>
@endsection