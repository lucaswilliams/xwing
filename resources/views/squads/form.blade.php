@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <form method="post" action="{{ URL::to('/squads') }}">
                    {{ csrf_field() }}

                    <?php
                    $name = '';
                    if(isset($squad->squad_id)) {
                        echo '<input type="hidden" name="squad_id" value="'.$squad->squad_id.'">';
                        $name = $squad->squad_name;
                    }
                    ?>
                    <div class="form-group">
                        <label for="squad_name" class="col-sm-2">Squad Name</label>
                        <div class="col-sm-10">
                            <input type="text" name="squad_name" id="squad_name" class="form-control" value="<?php echo $name; ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="squad_json" class="col-sm-2">Squad XWS<br /><small>Paste XWS here to overwrite your squad.</small></label>
                        <div class="col-sm-10">
                            <textarea name="squad_json" id="squad_json" class="form-control"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-10 col-sm-offset-2">
                            <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> Save</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-xs-12">
                <h1>Current Squad Settings</h1>
                <?php
                foreach($ships as $ship) {
                    echo '<div class="ship">';
                    echo '<h2>'.$ship->pilot_name.'</h2>';
                    echo '<h3>'.$ship->ship_name.'</h3>';
                    echo '<img src="'.url('/img/').'/'.$ship->pilot_image.'" class="ship-img">';
                    echo '<div class="upgrades">';
                    foreach($ship->upgrades as $upgrade) {
                        echo '<img src="'.url('/img/').'/'.$upgrade->upgrade_image.'" class="upgrade-img">';
                    }
                    echo '</div>';
                    echo '</div>';
                }
                ?>
            </div>
        </div>
    </div>
@endsection