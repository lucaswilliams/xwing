@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <form method="post" action="{{ URL::to('/profile') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="user_id" value="<?php echo $user->id; ?>" >

                    <div class="form-group">
                        <label for="name">Name:</label>
                        <input type="text" name="name" id="name" value="<?php echo $user->name; ?>" class="form-control">
                    </div>

                    <?php
                        if(strlen($user->google_id) > 0) {
                            echo '<div class="form-group"><p>You signed up using a Google account, with the email address '.$user->email.'</p></div>';
                            echo '<input type="hidden" name="email" value="'.$user->email.'" class="form-control">';
                        } elseif(strlen($user->facebook_id) > 0) {
                            echo '<div class="form-group"><p>You signed up using a Facebook account</p></div>';
                            echo '<input type="hidden" name="email" value="'.$user->email.'" class="form-control">';
                        } else {
                            //let them change their email and password, as these are used for logging in.
                            echo '<div class="form-group">
                                    <label for="email">Email:</label>
                                    <input type="email" name="email" id="email" value="'.$user->email.'" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="password">Password:</label>
                                    <input type="password" name="password" id="password" class="form-control">
                                    <label for="password_confirm">Password (confirm):</label>
                                    <input type="password" name="password_confirm" id="password_confirm" class="form-control">
                                </div>';
                        }
                    ?>
                    <div class="form-group">
                        <button class="btn btn-success" type="submit"><i class="fa fa-floppy-o"></i> Save</button>
                        <button class="btn btn-danger" type="reset"><i class="fa fa-trash-o"></i> Reset</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
