$(document).ready(function() {
    $('select:not(.noselect)').select2({
        width : '100%',
        minimumResultsForSearch: 1000000 /*arbitrarily large number, so it never shows */
    });

    $('select.search').select2({
        width : '100%',
        minimumResultsForSearch: 1 /*small number so it literally always shows */
    });
});

$(document).on('change', '.squad', function(e) {
    $('#initiative_1').prop('checked', false);
    $('#initiative_2').prop('checked', false);

    var _squad1 = $('#squad_1 option:selected');
    var _squad2 = $('#squad_2 option:selected');

    var _squad1p = _squad1.data('points');
    var _squad2p = _squad2.data('points');
    var _squad1f = _squad1.data('faction');
    var _squad2f = _squad2.data('faction');

    if(_squad2p < _squad1p) {
        //squad 2 has less points, they get initiative
        $('#initiative_2').prop('checked', true);
    } else if(_squad1p < _squad2p) {
        //squad 1 has less points, they get initiative
        $('#initiative_1').prop('checked', true);
    } else {
        //they are even, FFG rules state Imperials, so we'll do that
        //butt fuck it, these are radio buttons if you want to do your own
        if(_squad1f == '2') {
            //squad 1 is Imperial
            $('#initiative_1').prop('checked', true);
        } else if(_squad2f == '2') {
            //squad 2 is Imperial
            $('#initiative_2').prop('checked', true);
        } else {
            //butt fuck it, neither are Imperial; give it to the first one for reasons
            $('#initiative_1').prop('checked', true);
        }
    }
});