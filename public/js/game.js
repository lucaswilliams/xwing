Webcam.set({
    // live preview size
    width: 1280,
    height: 720,

    // device capture size
    dest_width: 1280,
    dest_height: 720,

    // final cropped size
    crop_width: 700,
    crop_height: 700,

    // format and quality
    image_format: 'jpeg',
    jpeg_quality: 90
});

Webcam.attach( '.board' );

function take_snapshot() {
    // take snapshot and get image data
    Webcam.snap( function(data_uri) {
        // display results in page
        _image = '<img src="' + data_uri + '">';
        console.log(_image);
        $('#controls').append(_image);
    } );
}

$(document).on('click', '.ship', function(e) {
    data = $.parseJSON(decodeURIComponent($(this).data('move')));
    $('.move-cell').removeClass('red').removeClass('white').removeClass('green');
    $.each(data, function(row, values) {
        $.each(values, function(column, intensity) {
            switch(intensity) {
                case 1 : _cl = 'white'; break;
                case 2 : _cl = 'green'; break;
                case 3 : _cl = 'red'; break;
                default : _cl = ''; break;
            }
            $('div[data-row="' + row + '"]').children('[data-col="' + column + '"]').addClass(_cl);
        });
    });
    $('#current_ship').val($(this).data('pilot'));
});

$(document).on('click', '.move-cell', function(e) {
    if($(this).hasClass('red') || $(this).hasClass('green') || $(this).hasClass('white')) {
        var _col = $(this).data('col');
        var _row = $(this).parent().data('row');
        var _move = '';
        var _int = '';
        if ($(this).hasClass('red')) {
            _int = 'stressful';
        }

        if ($(this).hasClass('white')) {
            _int = 'regular';
        }

        if ($(this).hasClass('green')) {
            _int = 'easy';
        }
        switch (_col) {
            case 0 :
                _move = 'turn left';
                break;
            case 1 :
                _move = 'bank left';
                break;
            case 2 :
                _move = 'straight';
                break;
            case 3 :
                _move = 'bank right';
                break;
            case 4 :
                _move = 'turn right';
                break;
            case 5 :
                _move = 'koiogran turn';
                break;
            case 6 :
                _move = 'segnor loop left';
                break;
            case 7 :
                _move = 'segnor loop right';
                break;
            case 8 :
                _move = 'tallon roll left';
                break;
            case 9 :
                _move = 'tallon roll right';
                break;
        }

        $('#movement-list').append('<p>' + $('#current_ship').val() + ' performs a ' + _row + ' ' + _move + ' (' + _int + ')</p>');
    }
});

$('#end-turn').click(function(e) {
    html2canvas($("#gamearea"), {
        onrendered: function(canvas) {
            theCanvas = canvas;
            document.body.appendChild(canvas);

            // Convert and download as image
            _image = Canvas2Image.convertToPNG(canvas);
            console.log(_image);
            $('#controls').append(_image);
            // Clean up
            document.body.removeChild(canvas);
        }
    });
    take_snapshot();
});