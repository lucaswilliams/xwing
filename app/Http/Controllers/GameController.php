<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class GameController extends Controller
{
    public function viewGame($game_id) {
        $games = DB::table('games')
            ->where('game_id', '=', $game_id)
            ->get();

        if(count($games) > 0) {
            $game = $games[0];
        } else {
            return view('errors.access', ['message' => 'That game does not exist.']);
        }

        return view('game.home', ['game' => $game]);
    }

    public function gameList() {
        $games = DB::table('games')
            ->get();

        return view('game.list', ['games' => $games]);
    }

    public function startGame() {
        $squads = array();
        $users = DB::table('users')
            ->join('squads', 'squads.user_id', '=', 'users.id')
            ->select('users.name', 'users.id')
            ->orderBy('users.name')
            ->distinct()
            ->get();

        foreach($users as $user) {
            $usersquads = DB::table('squads')
                ->where('user_id', '=', $user->id)
                ->orderBy('squads.squad_name')
                ->get();

            $squads[$user->name] = $usersquads;
        }

        return view('game.start', ['squads' => $squads]);
    }

    public function createGame(Request $request) {
        $game_id = DB::table('games')
            ->insertGetId([
                'squad_1_json' => $this->serialiseSquad($request->squad_1),
                'squad_2_json' => $this->serialiseSquad($request->squad_2),
                'game_initiative' => $request->initiative
            ]);

        DB::table('game_logs')
            ->insert([
                'game_id' => $game_id,
                'logs_time' => date('Y-m-d H:i:s'),
                'squad_1_json' => $this->serialiseSquad($request->squad_1),
                'squad_2_json' => $this->serialiseSquad($request->squad_2),
                'logs_description' => 'Game created.'
            ]);

        return redirect('/game/play/'.$game_id);
    }

    public function playGame($game_id) {
        $games = DB::table('games')
            ->join('game_logs', 'game_logs.game_id', '=', 'games.game_id')
            ->where('games.game_id', '=', $game_id)
            ->select('games.game_id', 'games.game_phase', 'games.game_initiative', 'game_logs.squad_1_json', 'game_logs.squad_2_json')
            ->orderBy('game_logs.logs_time', 'desc')
            ->get();

        if(count($games) > 0) {
            $game = $games[0];
        } else {
            return view('errors.access', ['message' => 'That game does not exist.']);
        }

        return view('game.simple', ['game' => $game]);
    }

    public function updateGame(Request $request) {
        //
    }

    private function serialiseSquad($squad_id) {
        $squads = DB::table('squads')
            ->where('squad_id', '=', $squad_id)
            ->get();

        if(count($squads) > 0) {
            $squad = $squads[0];

            $pilots = DB::table('pilots')
                ->join('ships', 'ships.ship_id', '=', 'pilots.ship_id')
                ->join('squad_pilots', 'squad_pilots.pilot_id', '=', 'pilots.pilot_id')
                ->where('squad_pilots.squad_id', '=', $squad->squad_id)
                ->select()
                ->get();

            foreach($pilots as $pilot) {
                $upgrades = DB::table('upgrades')
                    ->join('squad_upgrades', 'squad_upgrades.upgrade_id', '=', 'upgrades.upgrade_id')
                    ->where('squad_upgrades.spilot_id', '=', $pilot->spilot_id)
                    ->select()
                    ->get();

                $pilot->upgrades = $upgrades;
            }

            $squad->pilots = $pilots;
        }

        return json_encode($squad);
    }
}
