<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{
    public function showProfile($id = 0) {
        if($id == 0) {
            //User's own profile
            $id = Auth::user()->id;
            $admin = 0;
        } else {
            //Someone else's.  Need to be an admin
            if(Auth::user()->user_level == 1) {
                $admin = 1;
            } else {
                return view('errors.access', ['message' => 'You cannot edit other users\' profiles.']);
            }
        }
        $user = DB::table('users')
            ->where('id', '=', $id)
            ->get()[0];

        return view('auth.profile', ['user' => $user]);
    }

    public function saveProfile(Request $request) {
        DB::table('users')
            ->where('id', '=', $request->user_id)
            ->update([
                'name' => $request->name,
                'email' => $request->email
            ]);

        return redirect('/home')->with('success', 'Your profile has been updated successfully');
    }
}
