<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class SquadController extends Controller
{
    public function getList() {
        if(Auth::user()->level == 1) {
            $squads = DB::table('squads')
                ->join('users', 'users.id', '=', 'squads.user_id')
                ->get();
        } else {
            $squads = DB::table('squads')
                ->join('users', 'users.id', '=', 'squads.user_id')
                ->where('user_id', '=', Auth::user()->id)
                ->get();
        }

        return view('squads.list', ['squads' => $squads]);
    }

    public function getForm($id = 0) {
        if($id > 0) {
            $squad = DB::table('squads')
                ->where('squad_id', '=', $id)
                ->get()[0];
        } else {
            $squad = (object)array(
                'squad_id' => 0,
                'squad_name' => ''
            );
        }

        $shipArray = array();
        $ships = DB::table('ships')
            ->join('pilots', 'pilots.ship_id', '=', 'ships.ship_id')
            ->join('squad_pilots', 'squad_pilots.pilot_id', '=', 'pilots.pilot_id')
            ->where('squad_id', '=', $id)
            ->get();

        foreach($ships as $ship) {
            $upgrades = DB::table('upgrades')
                ->join('squad_upgrades', 'squad_upgrades.upgrade_id', '=', 'upgrades.upgrade_id')
                ->where('squad_upgrades.spilot_id', '=', $ship->spilot_id)
                ->get();

            $ship->upgrades = $upgrades;
            $shipArray[] = $ship;
        }

        return view('squads.form', ['squad' => $squad, 'ships' => $shipArray]);
    }

    public function deleteSquad($id) {
        DB::table('squad_upgrades')
            ->where('squad_id', '=', $id)
            ->delete();

        DB::table('squad_pilots')
            ->where('squad_id', '=', $id)
            ->delete();

        DB::table('squads')
            ->where('squad_id', '=', $id)
            ->delete();
    }

    public function saveForm(Request $request) {
        $user = Auth::user();

        $squadData = array(
            'squad_name' => $request->squad_name,
            'user_id' => $user->id
        );

        $squad_id = $request->squad_id;
        if($squad_id > 0) {
            //update
            DB::table('squads')
                ->where('squad_id', '=', $request->squad_id)
                ->update($squadData);
        } else {
            //insert
            $squad_id = DB::table('squads')
                ->insertGetId($squadData);
        }

        if(strlen($request->squad_json) > 0) {
            $this->processSquad($request->squad_json, $squad_id);
        }

        return redirect('/squads');
    }

    private function processSquad($inputXWS, $squad_id) {
        DB::table('squad_upgrades')
            ->where('squad_id', '=', $squad_id)
            ->delete();

        DB::table('squad_pilots')
            ->where('squad_id', '=', $squad_id)
            ->delete();

        $xwsjson = json_decode($inputXWS, true);

        switch($xwsjson['faction']) {
            case 'rebel' :
                $faction = 1;
                break;
            case 'imperial' :
                $faction = 2;
                break;
            case 'scum' :
                $faction = 3;
                break;
            default :
                $faction = 0;
                break;
        }

        DB::table('squads')
            ->where('squad_id', '=', $squad_id)
            ->update([
                'squad_points' => $xwsjson['points'],
                'squad_faction' => $faction
            ]);

        foreach($xwsjson['pilots'] as $ship) {
            $pilot = DB::table('pilots')
                ->where('pilot_stub', '=', $ship['name'])
                ->get()[0];

            $pilotData = array(
                'squad_id' => $squad_id,
                'pilot_id' => $pilot->pilot_id
            );

            $pilot_id = DB::table('squad_pilots')
                ->insertGetId($pilotData);

            if(isset($ship['upgrades'])) {
                foreach ($ship['upgrades'] as $key => $value) {
                    foreach ($value as $val) {
                        $upgrade = DB::table('upgrades')
                            ->where('upgrade_stub', '=', $val)
                            ->get()[0];

                        $upgradeData = array(
                            'spilot_id' => $pilot_id,
                            'squad_id' => $squad_id,
                            'upgrade_id' => $upgrade->upgrade_id
                        );

                        DB::table('squad_upgrades')
                            ->insert($upgradeData);
                    }
                }
            }
        }
    }
}
