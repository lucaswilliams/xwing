<?php

Route::get('/', function () {
    if(Auth::check()) {
        //The user is logged in, home page.
        return redirect('/home');
    } else {
        //The user is not logged in, redirect to login
        return redirect('/login');
    }
});



Route::group(['middleware' => 'web'], function () {
    Route::auth();
    Route::get('/home', 'HomeController@index');
    Route::get('/auth/google', 'Auth\AuthController@redirectToProvider');
    Route::get('/auth/google/callback', 'Auth\AuthController@handleProviderCallback');

    //Profile
    Route::get('/profile', 'ProfileController@showProfile');
    Route::get('/profile/{id}', 'ProfileController@showProfile')->where('id', '[0-9]+');
    Route::post('/profile', 'ProfileController@saveProfile');

    //Ships
    Route::get('squads', 'SquadController@getList');
    Route::get('squads/add', 'SquadController@getForm');
    Route::get('squads/{id}', 'SquadController@getForm')->where('id', '[0-9]+');
    Route::get('squads/delete/{id}', 'SquadController@deleteSquad')->where('id', '[0-9]+');
    Route::post('squads', 'SquadController@saveForm');

    //Game
    Route::get('game', 'GameController@gameList');
    Route::get('game/new', 'GameController@startGame');
    Route::post('game/new', 'GameController@createGame');
    Route::get('game/play/{id}', 'GameController@playGame');
    Route::get('game/view/{id}', 'GameController@viewGame');
});